package ballinger_challenge;
//Class name must be "Main"
//Libraries included:
//json simple, guava, apache commons lang3, junit, jmock
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Main {
 private static class BerTag {
     Byte byte1;
     Byte byte2;

     BerTag(Byte byte1) {
         this.byte1 = byte1;
     }

     BerTag(Byte byte1, Byte byte2) {
         this.byte1 = byte1;
         this.byte2 = byte2;
     }

     @Override
     public int hashCode() {
         if (byte2 != null) {
             return ((byte1 & 0xff) << 8) | (byte2 & 0xff);
         } else {
             return byte1;
         }
     }
 }

 static Set<BerTag> berTags = new HashSet<>();

 static {
     berTags.add(new BerTag((byte) 0x9F, (byte) 0x10));
     berTags.add(new BerTag((byte) 0x9F, (byte) 0x1A));
 }

 static boolean isTag(byte byte1) {
     return berTags.contains(new BerTag(byte1));
 }

 static boolean isTag(byte byte1, byte byte2) {
     return berTags.contains(new BerTag(byte1, byte2));
 }

 public static void main(String[] args) {
     byte[] bytes = new byte[]{
         (byte) 0x9F ,(byte) 0x10 ,(byte) 0x07 ,(byte) 0x06 ,(byte) 0x0F ,(byte) 0x0A,
         (byte) 0x03 ,(byte) 0xA0 ,(byte) 0xA0 ,(byte) 0x00 ,(byte) 0x9F ,(byte) 0x1A,
         (byte) 0x02 ,(byte) 0x08 ,(byte) 0x40};

     for (int byteIndex = 0; byteIndex < bytes.length; ++byteIndex) {
         byte nextByte = bytes[byteIndex];
         if (isTag(nextByte)) {
             System.out.println("Tag: " + nextByte);

             int length = bytes[byteIndex + 1];
             System.out.println("Length: " + length);

             byte[] values = Arrays.copyOfRange(bytes, byteIndex + 2, byteIndex + 2 + length);
             System.out.println("Values: " + values);
         } else if (byteIndex < bytes.length - 1 && isTag(nextByte, bytes[byteIndex + 1])) {
             byte followingByte = bytes[byteIndex + 1];
             System.out.println("Tag: " + new byte[]{nextByte, followingByte});

             int length = bytes[byteIndex + 2];
             System.out.println("Length: " + length);

             byte[] values = Arrays.copyOfRange(bytes, byteIndex + 3, byteIndex + 3 + length);
             System.out.println("Values: " + values);
         }
     }
 }
}
